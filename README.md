Парсер данных с веб-сайта https://spravkainform.ru
==================================================

Парсит данные с веб-сайта https://sparvkainform.ru по заданной категории и сохраняет эти данные в CSV-файл.



Установка
---------

Данный парсер не требует какой-либо установки.



Использование
-------------

Пример использования:

    require_once 'src/Parser.php';
    /* 
    Вызов без аргументов приведет к парсингу данных из URL https://spravkainform.ru/russia/rostov-na-donu/parikmaherskie-salony  и сохранению этих данных в файл output-<время в миллисекундах>.csv
    */
    $parser = new Parser();
    
    /*
    Тот же самый результат, что и выше, но данные будут сохранены в файле my-output.csv.
    */
    $parser = new Parser('my-output.csv');

    /*
    Класс MyOutputSchema должен наследовать от абстрактного класса WebsiteSchema.
    */
    require_once 'src/MyOutputSchema.php';
    $schema = new MyOutputSchema();
    $parser = new Parser('my-output.csv', $schema);
