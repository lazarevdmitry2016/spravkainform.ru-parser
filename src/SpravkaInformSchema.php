<?php
require_once 'WebsiteSchema.php';
 
class SpravkaInformSchema extends WebsiteSchema {
    protected $startURL = 'https://spravkainform.ru/russia/rostov-na-donu/parikmaherskie-salony';
    protected $rootPaginatorSelector = '.si-pager';
    protected $currentPageSelector = '.si-pager-current';
    protected $targetPageSelector = 'a';
    protected $itemSelector = '.category-companies-list-company-title>a';
    protected $itemDataSelectors = array(
        //'.si-linktitle-name',
	'.title',
        '.data-description .company-data-field-value',
        '.vote .comment',
        '.vote>.num-value',
        '.company-services>li',
        '.company-social a',
        '.company-site a',
        '.data-schedule>.company-data-field-value',
        '.company-phone',
        '.company-data-field-value>meta',
        '.company-title-desc>h2',
        '.company-title.title',
        
    );
}