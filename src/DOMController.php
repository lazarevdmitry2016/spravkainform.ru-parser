<?php

require_once 'vendor/simple_html_dom.php';
class DOMController {
    public function __construct($html){
        $this->html = str_get_html($html);
        if (gettype($this->html) != 'object'){
            $this->html = '';
        }
    }

    public function getHtml(){
        return $this->html;
    }
    public function find($what, $how_many = null){
        if (is_null($how_many)){
            return $this->html->find($what);
        } else {
            return $this->html->find($what, $how_many);
        }
        
    }
    public function hasData(){
        if ($this->html == ''){
            return false;
        }
        return true;
    }
}