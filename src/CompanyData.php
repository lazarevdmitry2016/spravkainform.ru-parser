<?php

class CompanyData {
    protected $fields = array();

    /**
     * @param   $field  string
    */
    public function addField(string $field){
        $this->fields[] = $field;
    }
    
    /**
     * @return  fields  string[]
    */
    public function asArray(){
        return $this->fields;
    }
}