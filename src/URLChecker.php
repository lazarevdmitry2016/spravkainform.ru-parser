<?php

class URLChecker {

    /**
     * @param   $string     string
     * @return  boolean
     */
    public static function is_url(string $string){
        if (count(parse_url($string)) <= 1){
            return false;
        } 
        return true;
    }
}