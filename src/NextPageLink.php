<?php

class NextPageLink {
    protected $url = '';
    protected $exists = true;

    /**
     * @param   $url    string
     */
    function __construct(string $url = null){
        // TODO
        if (is_null($url)){
            $this->exists = false;
        } else {
            $this->exists = true;
        }
        $this->url = $url;
    }

    /**
     * @return  url     string
     */
    public function getURL(){
        return $this->url;
    }
    public function isExists(){
        return $this->exists;
    }
}