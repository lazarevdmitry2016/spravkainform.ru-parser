<?php

class URLHealer {
    protected static $base_url = '';
    protected static $components = array();

    public static function setBaseURL($url){
        self::$components = parse_url($url);
    }
    /**
     * @param   $url    string
     * @return  $url    string
     */
    public static function heal($url){
        $components = parse_url($url);
        $components['scheme'] = self::$components['scheme'];
        $components['host'] = self::$components['host'];
        return "{$components['scheme']}://{$components['host']}{$components['path']}";
    }
}