<?php

abstract class WebsiteSchema {
    public function __construct($url = null){
    	   if (!is_null($url)){
	      $this->startURL = $url;
	   }
    }

    /**
     * @return  $startURL    string
     */
    public function getStartURL(){
        return $this->startURL;
    }

    /**
     * @return  $nextPageSelector   string
     */
    public function getNextPageSelector(){
        return $this->nextPageSelector;
    }

    /**
     * @return  $itemSelector   string
     */
    public function getItemSelector(){
        return $this->itemSelector;
    }

    /**
     * @return  $itemDataSelectors  string[]
     */
    public function getDataSelectors(){
        return $this->itemDataSelectors;
    }
    /**
     * @return  $itemDataSelectors  string
     */
    public function getRootSelector(){
        return $this->rootPaginatorSelector;
    }

    /**
     * @return  $itemDataSelectors  string
     */
    public function getCurrentPageSelector(){
        return $this->currentPageSelector;
    }

    /**
     * @return  $itemDataSelectors  string
     */
    public function getTargetPageSelector(){
        return $this->targetPageSelector;
    }
    
}