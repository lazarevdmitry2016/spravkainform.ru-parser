<?php
require_once 'CatalogPage.php';
require_once 'URLChecker.php';
require_once 'CURLController.php';
require_once 'WrongURLError.php';

class Website {
    protected $url = '';

    /**
     * @param   $url    string
     */
    public function __construct($url){
        $this->url = $url;
    }

    /**
     * This method downloads a catalog page according to the $page_url argument
     * 
     * @param   $page_url       NextPageLink      URL of a catalog page
     * @return  $catalogPage        CatalogPage         catalog page from $page_url
    */
    public function loadCatalogPage($page_url){
        $url = $page_url->getURL();
        if (!URLChecker::is_url($url)) {
            throw new WrongURLError("Wrong URL: $url");
        }
        $html = CURLController::downloadPage($url);
        return new CatalogPage($html);
    }
}