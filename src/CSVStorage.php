<?php
require_once 'IStorage.php';

class CSVStorage implements IStorage {
    protected $file = null;
    
    /**
     * @param   $path   string
    */
    function __construct(string $path = null){
        if (is_null($path)){
            $filePath = 'output-'.(time()).'.csv';
        } else {
            $filePath = $path;
        }
        $this->file = fopen($filePath, 'w+');
    }
    
    /**
     * @param   $data   CompanyData
     * 
    */
    function writeCompanyData(CompanyData $data){
        $d = $data->asArray();
        fputcsv($this->file, $d, ';');
    }

    /**
     * 
    */
    function close(){
        fclose($this->file);
    }
}