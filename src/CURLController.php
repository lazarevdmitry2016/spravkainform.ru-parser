<?php

class CURLController {
    /**
     * @param   $url    string
     * @return  $response   string
     */
    public static function downloadPage(string $url){
        $c = curl_init($url);
        if (!$c) throw new CURLError("Unable to download the page: $url");
        curl_setopt($c,CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($c);
        curl_close($c);
        return $response;
    }
}