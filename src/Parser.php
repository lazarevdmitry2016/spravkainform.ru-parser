<?php
require_once 'SpravkaInformSchema.php';
require_once 'CSVStorage.php';
require_once 'Website.php';
require_once 'WebsiteError.php';
require_once 'NextPageLink.php';
require_once 'URLHealer.php';

class Parser {
    /**
     * @param   $savePath   string      Path to a file where the data will be saved
     * @param   $schema     string      Target website schema
     * 
     */
    public function __construct($savePath = null, $schema = null)
    {
        echo "Starting the script.\n";
        $storage = new CSVStorage($savePath); 
        if (is_null($schema)){
            $schema = new SpravkaInformSchema();
        }
        $url = $schema->getStartURL();    // string
        URLHealer::setBaseURL($url);
        echo "Initializing the storage.\n";
        $website = new Website($url);
        $nextPageLink = new NextPageLink($url);

        while ($nextPageLink->isExists()){
            try {
                echo "Trying to download catalog page: {$nextPageLink->getURL()}.\n";
                $catalogPage = $website->loadCatalogPage($nextPageLink);
            } catch (WebsiteError $e){
                $storage->close();
                echo "$e->message\n";
                return;
            }
            echo "Download of the catalog page was successful.\n";

            echo "Finding the companies pages.\n";
            $companyPages = $catalogPage->findCompanyPages($schema->getItemSelector());
            
            foreach ($companyPages as $companyPage){
                echo "Finding the company's data.\n";
                $companyData = $companyPage->findCompanyData($schema->getDataSelectors());
                echo "Writing the company's data to the storage.\n";
                $storage->writeCompanyData($companyData);
            }
            echo "Finding a link to the next catalog page.\n";
            $nextPageLink = $catalogPage->findNextPageLink($schema);
            if ($nextPageLink->isExists()){
                echo "The link is: {$nextPageLink->getURL()}\n";
            } else {
                echo "No links.";
            }
        }
        echo "Closing the storage.\n";
        $storage->close();
        echo "Finishing the script.\n";
    }
}