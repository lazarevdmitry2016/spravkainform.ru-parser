<?php
require_once 'CompanyPage.php';
require_once 'NextPageLink.php';
require_once 'WebsiteSchema.php';
require_once 'DOMController.php';

class CatalogPage {
    protected $html = '';

    /**
     * @param   $html   string
    */
    function __construct(string $html){
        $this->html = new DOMController($html);
    }

    /**
     * @param   $selector       string
     * @return  $companyPages   array of CompanyPage
    */
    function findCompanyPages(string $selector){
        $temp = $this->html->find($selector);
        $companyPages = [];
        if (is_null($temp)){
            return $companyPages;
        }
        foreach($temp as $t){
            $companyPages[] = new CompanyPage(URLHealer::heal($t->href));
        }
        return $companyPages;
    }

    /**
     * @param   $root       string      selector for the root element
     * @param   $current    string      selector for the current element
     * @return  $nextPageLink   NextPageLink|null
    */
    function findNextPageLink(WebsiteSchema $schema){
        $rootSelector = $schema->getRootSelector();
        $currentSelector = $schema->getCurrentPageSelector();
        $targetSelector = $schema->getTargetPageSelector();

        $currentPageNode = $this->html->find($currentSelector,0);
        if (is_null($currentPageNode)){
            return new NextPageLink();
        }

        $nextPageNode = $currentPageNode->next_sibling();
        if (is_null($nextPageNode)){
            return new NextPageLink();
        }

        $targetPageNode = $nextPageNode->find($targetSelector,0);
        if (is_null($targetPageNode)){
            return new NextPageLink();
        }
        $targetPageHref = $targetPageNode->href;
        if (is_null($targetPageNode->href)){
            return new NextPageLink();
        }
        $targetPageHref = URLHealer::heal($targetPageHref);
        return new NextPageLink($targetPageHref);
    }
}
