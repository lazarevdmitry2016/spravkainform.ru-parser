<?php
require_once 'CompanyData.php';

interface IStorage {
    function writeCompanyData(CompanyData $data);
    function close();
}