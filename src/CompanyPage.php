<?php
require_once 'DOMController.php';
require_once 'CURLController.php';
require_once 'CURLError.php';
require_once 'CompanyData.php';


class CompanyPage {
    protected $html = '';

    /**
     * @param   $url    string
    */
    function __construct(string $url){
        try {
            $html = CURLController::downloadPage($url);
            $this->html = new DOMController($html);
        } catch (CURLError $e){
            echo "CURLError: $e->message";
        }
    }

    /**
     * @param   $selectors      string[]
     * @return  $companyData    CompanyData
    */
    public function findCompanyData($selectors){
        $data = new CompanyData();
        foreach ($selectors as $selector)
        {
            if (is_string($selector)){
                $field = $this->getSelectorData($selector);
            } else {
                $field = '';
            }
            $data->addField($field);
        }
        return $data;
    }

    /**
     * @param   $selector   string
     * @return  $selectorData   string
    */
    protected function getSelectorData(string $selector){ // TODO
        if ($this->html->hasData()){
            $t = $this->html->find($selector); // array

            if (is_null($t) || is_bool($t) || count($t) < 1){
                return '';
            }

            $temp = [];
            foreach ($t as $x){
                if (!is_null($x->innertext)){
                    if (is_null($x->href)){
                        $temp[] = $x->innertext;
                    } else {
                        $temp[] = $x->innertext.":".$x->href;
                    }
                }
            }
            return implode(';',$temp);
        } else {
            return '';
        }
    }
}
